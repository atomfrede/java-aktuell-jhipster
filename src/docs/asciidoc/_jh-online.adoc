=== JHipster Online

Jennifer erklärt ihm, dass JHipster Online <<jhonline>> selbst mit JHipster entwickelt wird.
Mit JHipster Online kann man eine JHipster Anwendung konfigurieren und als Zip Archiv herunterladen (<<img-jh-online>>).
Wenn man seinen GitHub oder GitLab Account mit JHipster Online verbindet, dann können die Projekte direkt in einem Git Repository generiert werden.
Weiterhin kann man verschiedene Entitätsmodelle verwalten und editieren und diese direkt auf eine generierte Anwendung importieren.
JHipster Online erzeugt dabei einen Pull Request auf dem Projekt, sodass man als Entwickler alle Änderungen noch reviewen kann. 
Demnach ist es sehr einfach verschiedene Anwendungskonfigurationen und Datenmodelle zu testen.

Allerdings, so Jennifer, sollte man JHipster ebenfalls lokal installieren, da einige Funktionen wie beispielsweiße
Deployments bisher noch nicht von JHipster Online unterstützt werden. 
Jennifer gibt Samu noch den Tipp Docker und Docker Compose zu installieren, 
damit er die erzeugten Compose Skripte für Datenbanken verwenden kann und diese nicht lokal auf seinem System installieren muss.

[#img-jh-online] 
.Anwendungskonfiguration in JHipster Online
image::jhipster-online.png[JHipster Online]


